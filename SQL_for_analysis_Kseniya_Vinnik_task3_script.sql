SELECT country_region, calendar_year, channel_desc, amount_sold,
  by_channels ||' %' "%BY CHANNELS",
  previous_period ||' %'  "%PREVIOUS PERIOD",
  (by_channels-previous_period)||' %' "%DIFF"
FROM
    (
    SELECT country_region, calendar_year, channel_desc, amount_sold, by_channels,
      FIRST_VALUE(by_channels) OVER (PARTITION BY country_region, channel_desc  
      ORDER BY calendar_year ASC ROWS BETWEEN 1 PRECEDING AND CURRENT ROW) previous_period
    FROM
        (
        SELECT country_region, calendar_year, channel_desc, SUM(amount_sold) amount_sold,
          ROUND(100*SUM(amount_sold)/(SUM(SUM(amount_sold)) 
          OVER (PARTITION BY country_region, calendar_year)),2) by_channels
        FROM sales s
        JOIN customers c
          ON s.cust_id = c.cust_id
        JOIN countries co
          ON co.country_id = c.country_id
        JOIN times t
          ON s.time_id = t.time_id
        JOIN channels ch
          ON s.channel_id = ch.channel_id
        WHERE calendar_year IN ('1998','1999', '2000', '2001') 
          AND country_region IN ('Americas', 'Asia', 'Europe')
        GROUP BY country_region, calendar_year, channel_desc
        )
    ORDER BY country_region ASC, calendar_year ASC, channel_desc ASC
     )
WHERE calendar_year IN ('1999', '2000', '2001')
;


SELECT t.calendar_week_number, t.time_id, t.day_name, SUM(s.amount_sold) sales,
  SUM(SUM(s.amount_sold)) OVER (PARTITION BY t.calendar_week_number ORDER BY t.time_id ASC 
  RANGE UNBOUNDED PRECEDING) cum_sum,
  CASE day_name 
      WHEN 'Monday' THEN ROUND(AVG(SUM(amount_sold)) OVER (PARTITION BY t.calendar_week_number 
        ORDER BY t.time_id ASC RANGE BETWEEN INTERVAL '2' DAY PRECEDING AND
        INTERVAL '1' DAY FOLLOWING),2)
      WHEN 'Friday' THEN ROUND(AVG(SUM(amount_sold)) OVER (PARTITION BY t.calendar_week_number 
        ORDER BY t.time_id ASC RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND
        INTERVAL '2' DAY FOLLOWING),2)
      ELSE ROUND(AVG(SUM(amount_sold)) OVER (PARTITION BY t.calendar_week_number 
        ORDER BY t.time_id ASC RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND
        INTERVAL '1' DAY FOLLOWING),2) 
  END CENTERED_3_DAY_AVG
FROM sales s
JOIN times t
  ON s.time_id = t.time_id
WHERE t.calendar_week_number BETWEEN 49 AND 51
  AND calendar_year = '1999'
GROUP BY t.calendar_week_number, t.time_id, t.day_name
ORDER BY time_id
;