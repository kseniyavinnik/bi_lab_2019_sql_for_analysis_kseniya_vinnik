--Analyze max and sum sales by year 1999, all channels and 2 counties(Canada and USA)

SELECT NVL(channel_desc, 'TOTAL') country_name,
    MAX(AMOUNT_SOLD) max_sold,
    SUM(AMOUNT_SOLD) sum_sold
FROM sales s
  INNER JOIN customers cu ON s.cust_id=cu.cust_id
  INNER JOIN countries co ON cu.country_id=co.country_id
  INNER JOIN channels ch ON s.channel_id=ch.channel_id
  INNER JOIN times t ON s.time_id=t.time_id
WHERE calendar_year='1999' AND country_name='Canada' OR country_name='United States of America'
GROUP BY ROLLUP(channel_desc, country_name)
ORDER BY channel_desc
;
  
--Analyze max and sum sales by years for each country + max and sum sales just by countries

SELECT  NVL(TO_CHAR(calendar_year), 'ALL YEARS')  calendar_year,
    NVL(country_name, 'ALL COUNTRIES')  country_name,
    MAX(AMOUNT_SOLD) max_sold,
    SUM(AMOUNT_SOLD) sum_sold
FROM sales s
  INNER JOIN customers cu ON s.cust_id=cu.cust_id
  INNER JOIN countries co ON cu.country_id=co.country_id
    INNER JOIN times t ON s.time_id=t.time_id
GROUP BY CUBE(calendar_year, country_name)
ORDER BY calendar_year
;  

--Analyze max and sum sales by years, by years and countries + total numbers for all years and all countries

SELECT DECODE(GROUPING(calendar_year), 1, 'ALL YEARS', calendar_year) calendar_year,
    DECODE(GROUPING(country_name), 1, 'ALL COUNTRIES', country_name) country_name,
    MAX(AMOUNT_SOLD) max_sold,
    SUM(AMOUNT_SOLD) sum_sold,
    GROUPING(calendar_year) gr_year,
    GROUPING(country_name) gr_country
FROM sales s
  INNER JOIN customers cu ON s.cust_id=cu.cust_id
  INNER JOIN countries co ON cu.country_id=co.country_id
  INNER JOIN times t ON s.time_id=t.time_id
GROUP BY ROLLUP(calendar_year, country_name)
ORDER BY calendar_year, country_name
;  

---Analyze avg sales by all years, all_channels and all categories
--ONLY FOR promo_categories internet AND TV

SELECT DECODE(GROUPING(calendar_year), 1, 'ALL YEARS', calendar_year) calendar_year ,
    DECODE(GROUPING(channel_desc), 1, 'ALL CHANNELS', channel_desc) channel,
    DECODE(GROUPING(promo_category), 1, 'ALL PROMO CATEGORIES', promo_category) promo_category,
    ROUND(AVG(AMOUNT_SOLD)) avg_sold,
    GROUPING_ID(calendar_year) gr_year,
    GROUPING_ID(channel_desc) gr_channel,
    GROUPING_ID(promo_category) gr_promo,
    GROUPING_ID(calendar_year, channel_desc, promo_category) gr_all
FROM sales s
  INNER JOIN promotions p ON s.promo_id=p.promo_id
  INNER JOIN channels ch ON s.channel_id=ch.channel_id
  INNER JOIN times t ON s.time_id=t.time_id
WHERE promo_category IN ('internet', 'TV')
GROUP BY CUBE(calendar_year, channel_desc, promo_category)
  HAVING GROUPING_ID(calendar_year, channel_desc, promo_category)>0
ORDER BY channel_desc, promo_category, calendar_year
;  

--Show number of sold products and SUM sales by months of 2000 year and countries
-- + number of sold products and SUM sales for each of these months
--By using HAVING clause with GROUP_ID We avoid dublicate rows

SELECT calendar_month_desc month,
    NVL(country_name, 'ALL COUNTRIES') country_name,
    COUNT(AMOUNT_SOLD) avg_sold,
    SUM(AMOUNT_SOLD) sum_sold,
    GROUP_ID()
FROM sales s
  INNER JOIN channels ch ON s.channel_id=ch.channel_id
  INNER JOIN times t ON s.time_id=t.time_id
  INNER JOIN customers cu ON s.cust_id=cu.cust_id
  INNER JOIN countries co ON cu.country_id=co.country_id 
WHERE calendar_year='2000'
GROUP BY calendar_month_desc, ROLLUP(calendar_month_desc,country_name)
  HAVING GROUP_ID()<1
ORDER BY calendar_month_desc, country_name
;  

--Show sales by three different groupings: 
--months of 2001, countries(Only'United States of America' and 'Australia') and promo categories

SELECT NVL(calendar_month_desc, ' ') month ,
    NVL(country_name, ' ') country_name,
    NVL(promo_category, ' ')promo_category,
    SUM(AMOUNT_SOLD) sales
FROM sales s
  INNER JOIN promotions p ON s.promo_id=p.promo_id
  INNER JOIN customers cu ON s.cust_id=cu.cust_id
  INNER JOIN countries co ON cu.country_id=co.country_id
  INNER JOIN times t ON s.time_id=t.time_id
WHERE country_name IN ('United States of America', 'Australia')
  AND calendar_year='2001'
GROUP BY GROUPING SETS(calendar_month_desc, country_name, promo_category)
ORDER BY calendar_month_desc, country_name, promo_category
;   