SELECT * FROM
  (
  SELECT prod_name, calendar_quarter_desc, amount_sold  
  FROM products p
  INNER JOIN sales s
    ON p.prod_id=s.prod_id
  INNER JOIN times t
    ON s.time_id=t.time_id
  INNER JOIN customers c
    ON s.cust_id=c.cust_id
  INNER JOIN countries co
    ON c.country_id=co.country_id
  WHERE country_subregion='Asia' AND prod_category='Photo' AND calendar_year='2000'
  )
PIVOT
(SUM(amount_sold) FOR calendar_quarter_desc IN ('2000-01' as Q1,'2000-02' as Q2,'2000-03' as Q3,'2000-04' as Q4))
ORDER BY prod_name
;


SELECT NVL(prod_name,'TOTAL'), SUM(Q1) Q1, SUM(Q2) Q2, SUM(Q3) Q3, SUM(Q4) Q4,
(NVL(SUM(Q1),0)+NVL(SUM(Q2),0)+NVL(SUM(Q3),0)+NVL(SUM(Q4),0)) YEAR_SUM
FROM
  (
  SELECT prod_name, calendar_quarter_desc, amount_sold  
  FROM products p
  INNER JOIN sales s
    ON p.prod_id=s.prod_id
  INNER JOIN times t
    ON s.time_id=t.time_id
  INNER JOIN customers c
    ON s.cust_id=c.cust_id
  INNER JOIN countries co
    ON c.country_id=co.country_id
  WHERE country_subregion='Asia' AND prod_category='Photo' AND calendar_year='2000'
  )
PIVOT
(SUM(amount_sold) FOR calendar_quarter_desc IN ('2000-01' as Q1,'2000-02' as Q2,'2000-03' as Q3,'2000-04' as Q4))
GROUP BY ROLLUP(prod_name)
ORDER BY prod_name
;

SELECT channel_desc, cust_last_name, cust_first_name, SUM(amount_sold) amount_sold
FROM
  (
  SELECT channel_desc, c.cust_id, cust_last_name, cust_first_name, SUM(amount_sold) amount_sold,
  RANK()OVER(PARTITION BY ch.channel_desc ORDER BY SUM(amount_sold) DESC) as rank_channel
  FROM channels ch
  INNER JOIN sales s
    ON s.channel_id=ch.channel_id
  INNER JOIN customers c
    ON c.cust_id=s.cust_id
  GROUP BY channel_desc, cust_last_name, cust_first_name, c.cust_id
  )
WHERE rank_channel<=5
GROUP BY ROLLUP(channel_desc,(cust_last_name, cust_first_name)) 
ORDER BY channel_desc
;

SELECT channel_desc, cust_id, cust_last_name, cust_first_name, y98+y99+y01 amount_sold
FROM
  (
  SELECT channel_desc, cust_id, cust_last_name, cust_first_name, y98, y99, y01,
  RANK() OVER (PARTITION BY channel_desc ORDER BY Y98 DESC NULLS LAST) rank98,
  RANK() OVER (PARTITION BY channel_desc ORDER BY Y99 DESC NULLS LAST) rank99,
  RANK() OVER (PARTITION BY channel_desc ORDER BY Y01 DESC NULLS LAST) rank01
  FROM
  (
    SELECT channel_desc, calendar_year, c.cust_id, cust_last_name, cust_first_name, amount_sold
    FROM channels ch
    INNER JOIN sales s
      ON s.channel_id=ch.channel_id
    INNER JOIN customers c
      ON c.cust_id=s.cust_id
    INNER JOIN times t
      ON s.time_id=t.time_id
    )
  PIVOT
  (SUM(amount_sold) FOR calendar_year IN ('1998' as y98,'1999' as y99,'2001' as y01))
  )
WHERE rank98<301 AND rank99<301 AND rank01<301
  AND y98 IS NOT NULL AND y99 IS NOT NULL AND y01 IS NOT NULL
ORDER BY y98+y99+y01 DESC
;



