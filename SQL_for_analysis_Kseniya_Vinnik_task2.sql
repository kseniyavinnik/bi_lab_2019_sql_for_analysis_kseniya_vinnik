SELECT DECODE(GROUPING(calendar_month_desc), 1, 'GRAND TOTAL', calendar_month_desc) year_month ,
    CASE WHEN GROUPING(calendar_month_desc)=0 AND  GROUPING(channel_desc)=1 THEN 'Total by Channels'
       ELSE channel_desc END channel,
    CASE WHEN GROUPING(channel_desc)=0 AND  GROUPING(country_name)=1 THEN channel_desc||' Total by States'
       ELSE country_name END country,
    ROUND(MAX(AMOUNT_SOLD)) max_sales$,
    ROUND(MIN(AMOUNT_SOLD)) min_sales$,
    ROUND(SUM(AMOUNT_SOLD)) sales$
FROM sales s
  INNER JOIN channels ch ON s.channel_id=ch.channel_id
  INNER JOIN customers cu ON s.cust_id=cu.cust_id
  INNER JOIN countries co ON cu.country_id=co.country_id
  INNER JOIN times t ON s.time_id=t.time_id
WHERE  calendar_month_desc='2000-12'
  AND channel_desc IN ('Internet', 'Direct Sales')
GROUP BY ROLLUP(calendar_month_desc, channel_desc, country_name)
;  