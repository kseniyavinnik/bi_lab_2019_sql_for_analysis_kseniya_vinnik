--1.1	SELECT ALL DIRECT SUBORDINATES OF STEVEN KING 

SELECT employee_id, manager_id, first_name, last_name
FROM HR.employees
WHERE LEVEL = 2
START WITH employee_id = 100
CONNECT BY PRIOR employee_id = manager_id
;

--1.2	GENERATE SEQUENTIAL NUMBERS FROM 1 TO 100

SELECT LEVEL L
FROM dual
CONNECT BY LEVEL <= 100
;

--1.3	GENERATE DATES FROM NOW (SYSDATE) UNTILL THE END OF THE CURRENT YEAR. 

SELECT SYSDATE + LEVEL - 1 AS day
FROM dual
CONNECT BY TRUNC(SYSDATE, 'YEAR') = TRUNC((SYSDATE + LEVEL - 1), 'YEAR')
;

--1.4	PRINT LIST OF EMPLOYEES FOR KOCHAR NEENA IN A TREE-VIEW

SELECT LPAD(' ', 3*(LEVEL)-2, '-' )||first_name ||' ' || last_name                  
as empl
FROM employees
START WITH employee_id = 101
CONNECT BY PRIOR employee_id = manager_id
;

--1.5	SELECT ALL MANAGERS OF EMPLOYEE WITH PHONE_NUMBER= '650.505.2876'. DON'T SHOW THE ACTUAL EMPLOYEE. ORDER HIERARCHY FROM TOP LEVEL MANAGER

SELECT  employee_id, last_name
FROM HR.employees
WHERE phone_number != '650.505.2876'
START WITH phone_number = '650.505.2876'
CONNECT BY employee_id =PRIOR manager_id
ORDER BY LEVEL DESC
;

--1.6	RETURN MISSING NUMBERS

SELECT level L
FROM(
   SELECT MAX(row_n) max_n
   FROM SH.massive
   )
CONNECT BY level <= max_n 
  minus
SELECT row_n
FROM SH.massive
;

--1.7	FROM THE PREVIOUS TASK, RETURN THREE SMALLEST MISSING NUMBERS, BETWEEN OF EXISTING NUMBERS.

SELECT *
FROM (
SELECT min_n -1 + level L
FROM(
   SELECT MIN(row_n) min_n,
   MAX(row_n) max_n
   FROM SH.massive
   )
CONNECT BY level <= max_n - min_n + 1
  minus
SELECT row_n
FROM SH.massive)
WHERE ROWNUM<=3
;

--1.8	WRITE A QUERY THAT WILL RETURN EACH SYMBOL FROM THE STRING AS A SEPARATE ROW.

WITH expression_table AS(
   SELECT '2*((5+7)+2*(2+3))*8)+9' expr
   FROM DUAL)
SELECT regexp_substr(expr,'.', 1, level)
FROM expression_table
CONNECT BY regexp_substr(expr,'.', 1, level) is not null
;

--1.9	FOR EACH EMPLOYEE OF THE 4TH LEVEL LIST THE FULL PATH FROM AND TO THE ROOT MANAGER

SELECT CONNECT_BY_ROOT first_name first_name, CONNECT_BY_ROOT last_name last_name, LEVEL,
   sys_connect_by_path(first_name||' ' || last_name,'\') org
FROM employees e
WHERE LEVEL = 4
CONNECT BY PRIOR e.manager_id =  e.employee_id
ORDER BY 1
;

--1.10	FIND THE MANAGER WITH ONLY ONE SUBORDINATE, WHO, IN TURN, HAS NO SUBORDINATES

SELECT m_last_name
FROM(
   SELECT PRIOR last_name m_last_name,
     COUNT(employee_id) OVER (PARTITION BY manager_id) number_of_employees
   FROM HR.employees
   WHERE CONNECT_BY_ISLEAF =1
   START WITH employee_id = 100
   CONNECT BY PRIOR employee_id = manager_id
   ORDER BY first_name
   )
WHERE number_of_employees = 1
;

--2.1	REPORT 4 – SALES TOTALS FOR PHOTO PRODUCTS BY QUARTER

SELECT NVL(prod_name,'TOTAL') prod_name,
  SUM(Q1) Q1,
  SUM(Q2) Q2, SUM(Q2)-SUM(Q1) DELTA21, ROUND((SUM(Q2)-SUM(Q1))/SUM(Q1*100),4) "DELTA21 %",
  SUM(Q3) Q3, SUM(Q3)-SUM(Q2) DELTA32, ROUND((SUM(Q3)-SUM(Q2))/SUM(Q2*100),4) "DELTA32 %",
  SUM(Q4) Q4, SUM(Q4)-SUM(Q3) DELTA43, ROUND((SUM(Q4)-SUM(Q3))/SUM(Q3*100),4) "DELTA43 %",
  (NVL(SUM(Q1),0)+NVL(SUM(Q2),0)+NVL(SUM(Q3),0)+NVL(SUM(Q4),0)) YEAR_SUM
FROM
  (
  SELECT prod_name, calendar_quarter_desc, amount_sold  
  FROM products p
  INNER JOIN sales s
    ON p.prod_id=s.prod_id
  INNER JOIN times t
    ON s.time_id=t.time_id
  INNER JOIN customers c
    ON s.cust_id=c.cust_id
  INNER JOIN countries co
    ON c.country_id=co.country_id
  WHERE country_subregion='Asia' AND prod_category='Photo' AND calendar_year='2000'
  )
PIVOT
(SUM(amount_sold) FOR calendar_quarter_desc IN ('2000-01' as Q1,'2000-02' as Q2,'2000-03' as Q3,'2000-04' as Q4))
GROUP BY ROLLUP(prod_name)
ORDER BY prod_name
;
